import unittest

import Simple


class TestSimple(unittest.TestCase):
    def test_find_digits(self):
        result = Simple.Simple.find("87603259bbbde3b62c83362949f181d9", "pierre")

        self.assertTrue("7pierre991" in result)

    def test_find_front_letter(self):
        result = Simple.Simple.find("9eaf56e58064002f3f7defed332d4bb4", "pierre")

        self.assertTrue("LPierre7" in result)

    def test_find_camel_case(self):
        result = Simple.Simple.find("358bb1ad234e1f1e93e93c0dfc10fad9", "pierre")

        self.assertTrue("pIeRRE" in result)

    def test_digits(self):
        result = Simple.Simple.digits("87603259bbbde3b62c83362949f181d9", "pierre")

        self.assertEqual(result["text"], "7pierre991")

    def test_front_letter(self):
        result = Simple.Simple.front_letter("9eaf56e58064002f3f7defed332d4bb4", "pierre")

        self.assertEqual(result["text"], "LPierre7")

    def test_camel_case(self):
        result = Simple.Simple.camel_case("358bb1ad234e1f1e93e93c0dfc10fad9", "pierre")

        self.assertEqual(result["text"], "pIeRRE")


if __name__ == '__main__':
    unittest.main()
