# Python_Security_TP

My (Antoine BASLÉ) code for the Security TP for ESIEE's python course.

**Information**


- [ ] MDP.txt contains all hashes with their results from these methods
- [x] MD2.txt contains the list of hashes missing (can be used for testing)
- [ ] dico_animaux.txt contains the initial list of animals (deprecated)
- [x] dico_animaux_2.txt contains the fixed list of animals (used)

Each class has its own set of unit tests in their homonym test class file
Main.py is used to decode everything (see "General" below)

**How to use**

- Execute `main.py` to launch code
- Define a variable and assign it `Decoding.Decode()` object
    - By default, the decoding mode is _SIMPLE_
    - It can be changed by sending a value in the constructor (`Decode()`) from `Decoding.Modes` enum values
       - _SIMPLE_
       - _DICTIONARY_
       - _INCREMENTAL_
- Add hashes to decode in the `hashes` string variable
   - Separated from each other by a semi column
- Add usernames to test for simple mode in the `names` string variable
   - Separated from each other by a semi column
- Loop through all hashes in `hashes`
   - Execute dictionary decoding by doing `variable.decode(hash)`
   - Loop through all names in `names.txt`
      - Execute simple decoding by doing `variable.decode(hash, username)`
   - Execute incremental decoding by doing `variable.decode(hash)`

**General**

You can executes `main.py`, when doing so ; it will executes all three decoding methods for unresolved hash and all first names in the `names.txt` file.

**Concepts**


| Classes | File | Usage |
| ------ | ------ | ------ |
| Decode | Decoding.py | Call mode's class to decode, hash compare values and do the incremental decoding |
| Modes | Decoding.py | Enum all decoding modes |
| Simple | Simple.py | Use the find method to use all simple decoding methods (uses username sent) |
| Dictionary | Dictionary.py | Use the find method to use all dictionary decoding methods (uses dico_animaux.txt as dictionary) |