import math

import Decoding
import string


class Simple:
    """
    Class Simple

    Public methods : find() ; digits() ; front_letter() ; camel_case()
    Private method : __clock_counting()
    """

    # Current method tested
    current_index = 0
    methods = "digits;front_letter;camel_case"

    @staticmethod
    def find(input_hash, username):
        """
        Return the next method's result
        :param input_hash: hash to decode
        :param username: base username
        :return: str
        """

        result = {"code": 1, "text": "Default"}

        # Loop until all methods returned 0 (no problem but either results or everything tested)
        while result["code"] != 0:
            # Update current method index
            if Simple.current_index <= len(Simple.methods.split(";"))-1:
                # Get method dynamically from list
                method = getattr(Simple, Simple.methods.split(";")[Simple.current_index])
                Simple.current_index += 1
            # Looped through all methods
            else:
                Simple.current_index = 0
                return "Failure for hash " + str(input_hash) + " and student " + \
                       username.capitalize() + " as \"simple methods\" ; all nested methods failed"

            # Return method's result
            result = method(input_hash, username.lower())

        Simple.current_index = 0
        return "SUCCESS ! Decoded \"" + str(input_hash) + "\" as \"" + str(result["text"]) + "\" for \"" + username + \
               "\" using \"" + str(result["method"]) + "\" simple method"

    @staticmethod
    def digits(input_hash, username, front = False):
        """
        Try to decode input hash for the following pattern :
        username (lowercase or capitalized) + 0-4 digits in front or back
        :param input_hash: hash to decode
        :param username: base username
        :return: 0 if finished (found or all possibilities)
        """

        # One time full lower case and one time capitalized
        for i in range(2):
            # Up to 4 numbers
            for x in range(1, 5):
                # All possibilities : n+1
                for y in range(x+1):
                    # Left side : x-y
                    for left in range(int(math.pow(10, (x-y)))):
                        # Right side : y
                        for right in range(int(math.pow(10, y))):
                            result = username.lower()
                            # Assign capitalize to first name and not added letter from front_letter method
                            if i % 2 == 0 and not front:
                                result = username.capitalize()
                            elif i % 2 == 0:
                                capitalized = list(username)
                                capitalized[1] = capitalized[1].upper()
                                result = "".join(capitalized)

                            # Clear power of 0
                            if (x - y) == 0:
                                left = ""
                            if y == 0:
                                right = ""

                            # Missing 0s
                            if (len(str(left)) + len(str(right))) < x:
                                if len(str(left)) < (x - y):
                                    for _ in range((x - y) - len(str(left))):
                                        left = "0" + str(left)
                                elif len(str(right)) < y:
                                    for _ in range(y - len(str(right))):
                                        right = "0" + str(right)

                            # Affect value
                            result = str(left) + result + str(right)

                            # Decoded
                            if Decoding.Decode.to_md5(result) == input_hash:
                                return {"code": 0, "text": result, "method": "digits"}

        # All possibilities
        return {"code": 1, "text": "No match found"}

    @staticmethod
    def front_letter(input_hash, username):
        """
        Same as digits but with a letter in front of the name
        :param input_hash: hash to decode
        :param username: base username
        :return: 0 if finished (found or all possibilities)
        """

        digits = {"code": 1, "text": "No match found"}
        # Loop through all letters
        for i in string.ascii_letters:
            new_username = i + username
            digits = Simple.digits(input_hash, new_username, True)

            # Decoded
            if digits["code"] == 0:
                digits["method"] = "front_letter"
                break

        # All possibilities
        return digits

    @staticmethod
    def camel_case(input_hash, username):
        """
        Try to decode input hash for the following pattern :
        username randomly capitalized
        :param input_hash: hash to decode
        :param username: base username
        :return: 0 if finished (found or all possibilities)
        """

        count = 0
        # Gear until full caps
        while count < len(username):
            result = Simple.__clock_counting(input_hash, list(username), count, count)
            if result:
                return {"code": 0, "text": result, "method": "camel_case"}
            else:
                count += 1

        # All possibilities
        return {"code": 1, "text": "No match found"}

    @staticmethod
    def __clock_counting(input_hash, username, gear, caps_count):
        """
        Recursive method to caps all chars of a string
        :param input_hash: hash to decode
        :param username: base username
        :param gear: depth count
        :return: result of the current gear
        """

        # Recursive
        if gear > 0:
            count = 0
            original = list(username)

            # Change to upper some elements
            for x in range(len(username)):
                # Change x chars to upper is capacity not reached
                if count < caps_count:
                    username[x] = "".join(username).upper()[x]
                    count += 1

                # Recursive call
                result = Simple.__clock_counting(input_hash, username, gear-1, caps_count)

                # Decoded
                if result:
                    return result
                # Reset
                else:
                    count = 0
                    username = list(original)

            return result

        count = 0
        original = list(username)

        # Change to upper some elements
        for x in range(len(username)):
            # Change x chars to upper is capacity not reached
            if count < caps_count:
                username[x] = "".join(username).upper()[x]
                count += 1

            # Decoded
            if Decoding.Decode.to_md5("".join(username)) == input_hash:
                return "".join(username)
            # Reset
            else:
                count = 0
                username = list(original)

        return False
