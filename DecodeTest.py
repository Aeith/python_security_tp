import unittest

import Decoding


class TestDecode(unittest.TestCase):
    def test_init(self):
        default = Decoding.Decode()

        self.assertEqual(default.mode, Decoding.Modes.SIMPLE)

    def test_init_mode(self):
        dictionary = Decoding.Decode(Decoding.Modes.DICTIONARY)

        self.assertEqual(dictionary.mode, Decoding.Modes.DICTIONARY)

    def test_str(self):
        default = Decoding.Decode()

        self.assertEqual(default.__str__(), Decoding.Modes.SIMPLE)

    def test_str_mode(self):
        dictionary = Decoding.Decode(Decoding.Modes.DICTIONARY)

        self.assertEqual(dictionary.__str__(), Decoding.Modes.DICTIONARY)

    def test_get_mode(self):
        default = Decoding.Decode()

        self.assertEqual(default._get_mode(), Decoding.Modes.SIMPLE)

    def test_get_mode_mode(self):
        dictionary = Decoding.Decode(Decoding.Modes.DICTIONARY)

        self.assertEqual(dictionary._get_mode(), Decoding.Modes.DICTIONARY)

    def test_set_mode(self):
        default = Decoding.Decode()

        default._set_mode(Decoding.Modes.DICTIONARY)

        self.assertEqual(default.mode, Decoding.Modes.DICTIONARY)

    def test_set_mode_mode(self):
        dictionary = Decoding.Decode(Decoding.Modes.DICTIONARY)

        dictionary._set_mode(Decoding.Modes.SIMPLE)

        self.assertEqual(dictionary.mode, Decoding.Modes.SIMPLE)

    def test_decode(self):
        default = Decoding.Decode()

        result = default.decode("87603259bbbde3b62c83362949f181d9", "pierre")

        self.assertTrue("7pierre991" in result)

    def test_decode_mode(self):
        dictionary = Decoding.Decode(Decoding.Modes.DICTIONARY)

        result = dictionary.decode("b0af912fa5f0e7aa3ffadcd493299db1")

        self.assertTrue("chieneuqacam" in result)

    def test_to_md5(self):
        default = Decoding.Decode()

        self.assertEqual(default.to_md5("~_<"), "1b0fe1a5dc5f5cb9d02fe02c1d5837e5")


if __name__ == '__main__':
    unittest.main()
