import math

import Decoding


class Dictionary:
    """
    Class Dictionary

    Public methods : find() ; animal() ; double_animal() ; digits() ; leet_speak() ; double_lamina() ; double_laminalamina() ; lamina_different()
    """

    # Current method tested
    current_index = 0
    methods = "animal;double_animal;digits;leet_speak;double_lamina;double_laminalamina;lamina_different"

    @staticmethod
    def find(input_hash):
        """
        Return the next method's result
        :param input_hash: hash to decode
        :return: str
        """

        # In the class | Correct username
        result = {"code": 1, "text": "Not in the class"}

        # Loop until all methods returned 0 (no problem but either results or everything tested)
        while result["code"] != 0:
            # Update current method index
            if Dictionary.current_index <= len(Dictionary.methods.split(";"))-1:
                # Get method dynamically from list
                method = getattr(Dictionary, Dictionary.methods.split(";")[Dictionary.current_index])
                Dictionary.current_index += 1
            # Looped through all methods
            else:
                Dictionary.current_index = 0
                return "Failure for hash " + str(input_hash) + " as \"dictionary methods\" ; all nested methods failed"

            # Return method's result
            result = method(input_hash)

        Dictionary.current_index = 0
        return "SUCCESS ! Decoded \"" + str(input_hash) + "\" as \"" + str(result["text"]) + "\" using \""\
               + str(result["method"]) + "\" dictionary method"

    @staticmethod
    def animal(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        animal = {"code": 1, "text": "No match found"}
        with open("dico_animaux_2.txt", "r") as f:
            for line in f:
                for i in range(2):
                    result = line.strip("\ufeff").rstrip("\n")

                    # Capitalize
                    if i % 2 == 0:
                        result = result.capitalize()

                    # Decoded
                    if Decoding.Decode.to_md5(result) == input_hash:
                        animal = {"code": 0, "text": result, "method": "animal"}
                        break

        return animal

    @staticmethod
    def double_animal(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal + other animal
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        animal = {"code": 1, "text": "No match found"}
        with open("dico_animaux_2.txt", "r") as f:
            # Store all lines in list
            content = f.readlines()
            content = [x.strip("\ufeff").rstrip("\n") for x in content]

            # First animal
            for x in content:
                # Second animal
                for y in content:
                    # Decoded
                    if Decoding.Decode.to_md5(x+y) == input_hash:
                        animal = {"code": 0, "text": x+y, "method": "double_animal"}
                        break

        return animal

    @staticmethod
    def digits(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal (lowercase or capitalized) + 0-3 digits in front or back
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        with open("dico_animaux_2.txt", "r") as f:
            for line in f:
                # Remove trailing space
                line = line.strip("\ufeff").rstrip("\n")

                # One time full lower case and one time capitalized
                for i in range(2):
                    # Up to 3 numbers
                    for x in range(1, 4):
                        # All possibilities : n+1
                        for y in range(x+1):
                            # Left side : x-y
                            for left in range(int(math.pow(10, (x-y)))):
                                # Right side : y
                                for right in range(int(math.pow(10, y))):
                                    result = line.lower()
                                    if i % 2 == 0:
                                        result = line.capitalize()

                                    # Clear power of 0
                                    if (x - y) == 0:
                                        left = ""
                                    if y == 0:
                                        right = ""

                                    # Missing 0s
                                    if (len(str(left)) + len(str(right))) < x:
                                        if len(str(left)) < (x - y):
                                            for _ in range((x - y) - len(str(left))):
                                                left = "0" + str(left)
                                        elif len(str(right)) < y:
                                            for _ in range(y - len(str(right))):
                                                right = "0" + str(right)

                                    # Affect value
                                    result = str(left) + result + str(right)

                                    # Decoded
                                    if Decoding.Decode.to_md5(result) == input_hash:
                                        return {"code": 0, "text": result, "method": "digits"}

        # All possibilities
        return {"code": 1, "text": "No match found"}

    @staticmethod
    def double_lamina(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal => laminalamina
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        animal = {"code": 1, "text": "No match found"}
        with open("dico_animaux.txt", "r") as f:
            for line in f:
                # Remove trailing space
                line = line.strip("\ufeff").rstrip("\n")
                # Reverse and double
                line = line[::-1]+line[::-1]

                # Decoded
                if Decoding.Decode.to_md5(line) == input_hash:
                    animal = {"code": 0, "text": line, "method": "double_lamina"}
                    break

        return animal

    @staticmethod
    def double_laminalamina(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal animal2 => lamina2lamina
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        animal = {"code": 1, "text": "No match found"}
        with open("dico_animaux.txt", "r") as f:
            # Store all lines in list
            content = f.readlines()
            content = [x.strip("\ufeff").rstrip("\n") for x in content]

            # First animal
            for x in content:
                # Second animal (in reverse)
                for y in content:
                    # Reverse and double
                    line = x[::-1]+y[::-1]

                    # Decoded
                    if Decoding.Decode.to_md5(line) == input_hash:
                        animal = {"code": 0, "text": line, "method": "double_laminalamina"}
                        break

        return animal

    @staticmethod
    def leet_speak(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal => 4N1M4L (caps + vowel changed to digits)
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        animal = {"code": 1, "text": "No match found"}
        vowels = ("A", "E", "I", "O", "U", "Y")

        # Encode to remove trailing chars
        with open("dico_animaux_2.txt", "r", encoding="utf-8-sig") as f:
            for line in f:
                line = list(line.strip("\ufeff").rstrip("\n").upper())

                vowels_list = list()
                # Get all vowels
                for i in range(len(line)):
                    if line[i] in vowels:
                        vowels_list.append(i)

                original = list("".join(line))
                for v in vowels_list:
                    original[v] = "0"

                # Change all vowels
                for x in range(len(vowels_list)):
                    result = Dictionary.__replace_recursive(input_hash, original, vowels_list, x)

                    if result:
                        return result

        return animal

    @staticmethod
    def __replace_recursive(input_hash, animal, vowels_list, depth):
        """
        Recursive method to change vowel's by digit
        :param input_hash: hash to decode
        :param animal: animal in list form
        :param vowels_list: index of vowels in animal in list form
        :param depth: depth of the recursive method
        :return: bool True if decoded
        """

        # Recursive call for each vowel
        if depth > 0:
            for x in range(10):
                animal[vowels_list[depth]] = str(x)

                result = Dictionary.__replace_recursive(input_hash, animal, vowels_list, depth-1)
                if result:
                    return result

            animal[vowels_list[depth]] = "0"

        # Replace vowel with value
        for x in range(10):
            # Stack overflow
            if x + 1 == len(vowels_list):
                continue

            animal[vowels_list[depth]] = str(x)

            # Decoded
            if Decoding.Decode.to_md5("".join(animal)) == input_hash:
                return {"code": 0, "text": "".join(animal), "method": "leet_speak"}

        return False;

    @staticmethod
    def lamina_different(input_hash):
        """
        Try to decode input hash for the following pattern :
        animal => animal + other lamina
        :param input_hash: hash to decode
        :return: 0 if finished (found or all possibilities)
        """

        animal = {"code": 1, "text": "No match found"}
        with open("dico_animaux_2.txt", "r") as f:
            # Store all lines in list
            content = f.readlines()
            content = [x.strip("\ufeff").rstrip("\n") for x in content]

            # First animal
            for x in content:
                # Second animal (in reverse)
                for y in content:
                    # Decoded
                    if Decoding.Decode.to_md5(x+y[::-1]) == input_hash:
                        animal = {"code": 0, "text": x+y[::-1], "method": "lamina_different"}
                        break

        return animal
