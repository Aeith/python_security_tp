import Decoding

simple = Decoding.Decode()
dictionary = Decoding.Decode(Decoding.Modes.DICTIONARY)
incremental = Decoding.Decode(Decoding.Modes.INCREMENTAL)

result = {"dictionary": "", "incremental": "", "simple": ""}
with open("MDP2.txt", "r") as f:
    for li in f:
        # Remove trailing space
        h = li.rstrip("\n")

        print("Decoding hash " + h + " ...")

        # Dictionary
        result["dictionary"] = dictionary.decode(h)
        if "SUCCESS" in result["dictionary"]:
            print(result["dictionary"])
            continue

        # Simple
        if "SUCCESS" not in result["dictionary"]:
            with open("names.txt", "r") as f:
                for line in f:
                    # Remove trailing space
                    s = line.rstrip("\n")

                    print("For user " + s + " ...")

                    result["simple"] = simple.decode(h, s)
                    if "SUCCESS" in result["simple"]:
                        print(result["simple"])
                        break

        # Incremental
        if "SUCCESS" not in result["dictionary"] and "SUCCESS" not in result["simple"]:
            result["incremental"] = incremental.decode(h)
            if "Success" in result["incremental"]:
                print(result["incremental"])
                continue

        if "Success" not in result:
            print("All methods failed (incremental stopped for max capacity) for hash " + h)
