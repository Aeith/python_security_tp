from enum import Enum
import hashlib

import Simple
import Dictionary


class Modes(Enum):
    """
    Class Enum

    Used to enumerate all decoding modes
    """
    SIMPLE = 1
    DICTIONARY = 2
    INCREMENTAL = 3


class Decode:
    """
    Class Decode

    Base methods : __init__() ; __str__()
    Getters : _get_mode()
    Setters : _set_mode()
    Public method : decode()
    Private methods : __decode_simple() __decode_dictionary() __decode_incremental() __to_md5()
    """

    def __init__(self, mode=Modes.SIMPLE):
        """
        Constructor

        :param mode: Modes enum
        """
        self.mode = mode

    def __str__(self):
        """
        To String
        :return: string
        """
        return self.mode

    def _get_mode(self):
        """
        Getter
        :return: Modes enum
        """
        return self.mode

    def _set_mode(self, value):
        """
        Setter
        :param value: Modes enum
        :return: void
        """
        self.mode = value

    def decode(self, input_hash, username=""):
        """
        Use the decode method depending on the selected mode
        :param input_hash: str
        :param username: str
        :return: str
        """
        if self.mode == Modes.SIMPLE:
            return Decode.__decode_simple(input_hash, username)
        elif self.mode == Modes.DICTIONARY:
            return Decode.__decode_dictionary(input_hash)
        else:
            return Decode.__decode_incremental(input_hash)

    @staticmethod
    def __decode_simple(input_hash, username=""):
        """
        Use username to decode
        :param input_hash: str
        :param username:
        :return: str
        """

        return Simple.Simple.find(input_hash, username)

    @staticmethod
    def __decode_dictionary(input_hash):
        """
        Use dictionary to decode
        :param input_hash: str
        :return: str
        """

        return Dictionary.Dictionary.find(input_hash)

    @staticmethod
    def __decode_incremental(input_hash):
        """
        Use brute force to decode
        :param input_hash: str
        :return: str
        """

        count = 0
        password = input_hash
        # Loop until found OR stop at 5 chars (we don't have all that time)
        while Decode.to_md5(password) != input_hash and count+1 < 4:
            # Recursive ascii
            password = Decode.__geared_ascii(input_hash, list([chr(32) for x in range(count+1)]), count+1)

            if password:
                return "SUCCESS ! Decoded \"" + str(input_hash) + "\" as \"" + \
                       password + "\" using \"incremental\" method"
            else:
                count += 1

        # No more than 4 chars means success
        if count+1 < 4:
            return "SUCCESS ! Decoded \"" + str(input_hash) + "\" as \"" +\
                   str(password) + "\" using \"incremental method\""
        else:
            return "Failure for hash " + str(input_hash) + " as \"incremental method\" : reached maximum capacity."

    @staticmethod
    def __geared_ascii(input_hash, password, gear):
        """
        Recursive method used to increment ascii chars
        :param input_hash: hash to decode
        :param password: password
        :param gear: depth
        :return: bool True if found for this gear
        """

        # Recursive
        if gear > 1:
            for n in range(gear+1):
                for x in range(32, 127):
                    password[gear-1] = chr(x)

                    lower_gear = Decode.__geared_ascii(input_hash, password, gear-1)
                    if lower_gear:
                        return lower_gear

            return False

        # Loop through all ascii chars
        for x in range(32, 127):
            # Only change the geared char
            password[gear-1] = chr(x)

            # Decoded
            if Decode.to_md5("".join(password)) == input_hash:
                return "".join(password)

        return False

    @staticmethod
    def to_md5(password):
        """
        Hash input into using MD5 hash
        :param password: str
        :return: str
        """
        return hashlib.md5(str(password).encode('utf-8')).hexdigest()
