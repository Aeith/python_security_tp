import unittest

import Dictionary


class TestDictionary(unittest.TestCase):
    def test_find_animal(self):
        result = Dictionary.Dictionary.find("df90e13fa7699df8a377946815cf5dc4")

        self.assertTrue("lapin" in result)

    def test_find_double_animal(self):
        result = Dictionary.Dictionary.find("7e2856c59ec4de8f74d3dcdc525d859c")

        self.assertTrue("lapinlapin" in result)

    def test_find_digits(self):
        result = Dictionary.Dictionary.find("415e6dc0d68caa3208d2452a6c4d497a")

        self.assertTrue("12lapin3" in result)

    def test_find_double_lamina(self):
        result = Dictionary.Dictionary.find("42167255eb290439c4200edfe3639ab5")

        self.assertTrue("nipalnipal" in result)

    def test_find_double_laminalamina(self):
        result = Dictionary.Dictionary.find("8c2d7de7d1d8a939e2b590590c6868d1")

        self.assertTrue("nipalxadda" in result)

    def test_find_leet_speak(self):
        result = Dictionary.Dictionary.find("7f1508408f66fd547558e1c68c041cbf")

        self.assertTrue("L4P1N" in result)

    def test_find_lamina_different(self):
        result = Dictionary.Dictionary.find("9f0f2302f536b7f4d5be56cede64d7fd")

        self.assertTrue("lapineuqacam" in result)

    def test_animal(self):
        result = Dictionary.Dictionary.animal("df90e13fa7699df8a377946815cf5dc4")

        self.assertEqual(result["text"], "lapin")

    def test_double_animal(self):
        result = Dictionary.Dictionary.double_animal("7e2856c59ec4de8f74d3dcdc525d859c")

        self.assertEqual(result["text"], "lapinlapin")

    def test_digits(self):
        result = Dictionary.Dictionary.digits("415e6dc0d68caa3208d2452a6c4d497a")

        self.assertEqual(result["text"], "12lapin3")

    def test_double_laminalamina(self):
        result = Dictionary.Dictionary.double_laminalamina("8c2d7de7d1d8a939e2b590590c6868d1")

        self.assertEqual(result["text"], "nipalxadda")

    def test_double_lamina(self):
        result = Dictionary.Dictionary.double_lamina("42167255eb290439c4200edfe3639ab5")

        self.assertEqual(result["text"], "nipalnipal")

    def test_leet_speak(self):
        result = Dictionary.Dictionary.leet_speak("1e4a8547f392559b1d1c44b693e02b89")

        self.assertEqual(result["text"], "H7R1ND0LL5")

    def test_lamina_different(self):
        result = Dictionary.Dictionary.lamina_different("9f0f2302f536b7f4d5be56cede64d7fd")

        self.assertEqual(result["text"], "lapineuqacam")


if __name__ == '__main__':
    unittest.main()
